import * as _ from 'lodash'

export enum FilterCondition {
	GT = 'gt',
	GTE = 'gte',
	LT = 'lt',
	LTE = 'lte',
	EQ = 'eq',
	NEQ = 'neq'
}

export type FilterValueTypes = string | number | string[] | number[]

export interface FilterParam {
	k: string
	c: FilterCondition
	v: FilterValueTypes
}

export function isFilterCondition(value: unknown): value is FilterCondition {
	return typeof value === 'string' && Object.values(FilterCondition).includes(value as FilterCondition)
}

export function validateFilterParams(value: string): boolean {
	try {
		const filterBy = JSON.parse(value)

		if (!Array.isArray(filterBy)) {
			return false
		}

		return filterBy.every((filter: unknown) => {
			if (!_.isPlainObject(filter)) {
				return false
			}
			return isFilterCondition((filter as FilterParam).c)
		})
	} catch (error) {
		return false
	}
}

interface FilterPredicate {
	(value: any): boolean
}

interface LodashFilterFunction {
	(value: unknown, other: unknown): boolean
}

const predicationMap = new Map<FilterCondition, LodashFilterFunction>([
	[FilterCondition.GT, _.gt],
	[FilterCondition.GTE, _.gte],
	[FilterCondition.LT, _.lt],
	[FilterCondition.LTE, _.lte],
	[FilterCondition.EQ, _.isEqual],
	[FilterCondition.NEQ, (value: unknown, element: unknown): boolean => !_.isEqual(element, value)]
])

function generatePredicate(filterBy: FilterParam[]): FilterPredicate {
	return function (value): boolean {
		return filterBy.every((filter) => {
			if (Array.isArray(filter.v)) {
				return filter.v.some((el) => (predicationMap.get(filter.c) as LodashFilterFunction)(value[filter.k], el))
			}

			return (predicationMap.get(filter.c) as LodashFilterFunction)(value[filter.k], filter.v)
		})
	}
}

export function filter<T>(data: T[], filterBy?: FilterParam[]): T[] {
	return filterBy ? _.filter(data, generatePredicate(filterBy)) : data
}
