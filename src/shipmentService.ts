import faker from 'faker'
import { filter, FilterParam } from './utils'

export interface Shipment {
	id: string
	createdAt: number
	name: string
	contractor: Contractor
	status: Status
}

export interface Contractor {
	id: string
	name: string
}

export enum Status {
	PROCESSING = 1,
	SUSPENDED,
	PAID,
	DELIVERED,
	RETURNED
}

export interface ShipmentData {
	data: Shipment[]
	skip: number
	take: number
	total: number
}

function makeShipment(date?: Date, status?: Status): Shipment {
	return {
		id: faker.datatype.uuid(),
		createdAt: date?.getTime() || faker.date.recent(30).getTime(),
		name: faker.commerce.productName(),
		contractor: {
			id: faker.datatype.uuid(),
			name: faker.company.companyName()
		},
		status: status || faker.datatype.number({ min: 1, max: 5 })
	}
}

class ShipmentService {
	private data: Shipment[] = []

	// Limits for processing shipments (not delivered)
	private min: number = 300
	private max: number = 600

	// It's not much, but it's an honest work
	private updateRate: number = 4
	private loopIntervalId: NodeJS.Timer | undefined = undefined

	constructor() {
		this.seed()
	}

	start() {
		this.loopIntervalId = setInterval(this.loop.bind(this), 500)
	}

	stop() {
		if (this.loopIntervalId) {
			clearInterval(this.loopIntervalId)
		}
	}

	seed() {
		this.data = new Array(faker.datatype.number({ min: this.min, max: this.max })).fill(null).map(() => makeShipment())
	}

	loop() {
		// Update items
		const processingItems = this.data.filter((el) => ![Status.DELIVERED, Status.RETURNED].includes(el.status))

		for (let i = 0; i < Math.min(this.updateRate, processingItems.length); i++) {
			const item = processingItems[faker.datatype.number({ min: 0, max: processingItems.length })]
			if (!item) continue

			switch (item.status) {
				case Status.PROCESSING:
					item.status = Math.random() < 0.05 ? Status.SUSPENDED : Status.DELIVERED
					break
				case Status.SUSPENDED:
					if (Math.random() < 0.1) item.status = Status.RETURNED
					break
				case Status.PAID:
					item.status = Status.DELIVERED
					break
			}
		}

		// Add new items
		if ((Math.random() < 0.5 || processingItems.length < this.min) && processingItems.length < this.max) {
			for (let i = 0; i < Math.max(1, this.min - processingItems.length); i++) {
				this.data.push(makeShipment(new Date(), Status.PROCESSING))
			}
		}
	}

	async get(skip: number, take: number, filterParams?: FilterParam[]): Promise<ShipmentData> {
		let query = [...this.data].sort((a, b) => b.createdAt - a.createdAt)

		if (filterParams) {
			query = filter(query, filterParams)
		}

		return {
			data: query.slice(skip, skip + take),
			skip,
			take,
			total: query.length
		}
	}

	async resolve(id: string, action: 'pay' | 'return'): Promise<Shipment> {
		const item = this.data.find((el) => el.id === id)
		if (!item) throw new Error('Item does not exist')

		if (item.status === Status.RETURNED) throw new Error('Item already returned.')
		if (item.status !== Status.SUSPENDED) throw new Error('You can only resolve suspended items.')

		item.status = action === 'pay' ? Status.PAID : Status.RETURNED
		return item
	}
}

export default new ShipmentService()
