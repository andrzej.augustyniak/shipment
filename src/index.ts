import express from 'express'
import { default as cors } from 'cors'
import { body, param, query, validationResult } from 'express-validator'
import faker from 'faker'
import { default as shipmentService } from './shipmentService'
import { FilterCondition, validateFilterParams } from './utils'

const PORT = 8000
const IP = '127.0.0.1'

const app = express()
app.use(cors())
app.use(express.json())

shipmentService.start()

function sleep(delay: number = 0): Promise<void> {
	return new Promise((resolve) => setTimeout(resolve, delay))
}

app.get(
	'/api/cargo',
	[query('skip').optional().isInt(), query('take').optional().isInt(), query('filter').optional().isJSON().custom(validateFilterParams)],
	async (req: express.Request, res: express.Response) => {
		await sleep(faker.datatype.number({ min: 100, max: 1000 }))

		const errors = validationResult(req)
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() })
		}

		try {
			const filterParams = 'filter' in req.query ? JSON.parse(String(req.query.filter)) : undefined
			const skip = Number(req.query.skip) || 0
			const take = Number(req.query.take) || 20

			const data = await shipmentService.get(skip, take, filterParams)

			res.json(data)
		} catch (e) {
			res.status(500).json(e.message)
		}
	}
)

app.post(
	'/api/cargo/:action',
	[
		body('id').exists().isUUID(),
		param('action')
			.exists()
			.custom((val) => ['pay', 'return'].includes(val))
	],
	async (req: express.Request, res: express.Response) => {
		await sleep(faker.datatype.number({ min: 100, max: 1000 }))

		const errors = validationResult(req)
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() })
		}

		try {
			const { data } = await shipmentService.get(0, 1, [{ k: 'id', c: FilterCondition.EQ, v: req.body.id }])
			if (data.length === 0) return res.status(404).send()

			res.json(await shipmentService.resolve(req.body.id, req.params.action as 'pay' | 'return'))
		} catch (e) {
			res.status(500).json(e.message)
		}
	}
)

app.listen(PORT, IP, () => {
	console.log(`API listening on ${IP}:${PORT}`)
})
