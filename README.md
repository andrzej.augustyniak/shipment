# shipment-api

## Run the API
```
npm run start
```
*by default runs at 127.0.0.1:8000*

## Notable interfaces

```ts
export interface Shipment {
	id: string
	createdAt: number
	name: string
	contractor: Contractor
	status: Status
}
```
```ts
export enum Status {
	PROCESSING = 1,
	SUSPENDED,
	PAID,
	DELIVERED,
	RETURNED
}
```

## Routes

### **GET api/cargo/**

*Get items. Item are always sorted by date descending*

### query params (all optional):
- `skip?: number = 0`
- `take?: number = 20`
- `filter?: string` - stringified json array of type `Array<FilterParam>`, where `key (k)` is the `Shipment's` property we want to filter with the `value (v)` using specificed `condition (c)`. You can specify as many conditions as you like. By default all the condition must be met for the item to be returned. To specify multiple `OR` conditions, pass array as `value (v)` (see example below).

```ts
export interface FilterParam {
	k: string
	c: FilterCondition
	v: FilterValueTypes
}
```
```ts
export enum FilterCondition {
	GT = 'gt', 		// greater than
	GTE = 'gte',	// greater than or equal
	LT = 'lt',		// lower than
	LTE = 'lte',	// lower than or equal
	EQ = 'eq',		// equal
	NEQ = 'neq'		// not equal
}
```
```ts
export type FilterValueTypes = string | number | string[] | number[]
```


### usage:
```
api/cargo?skip=30&take=50&filter=[{"k":"status","c":"eq","v":4},{"k":"createdAt","c":"gt","v":1635848199000}]
```
*retrieve 50 items starting from position 30 that have status of 4 (see enum Status) AND createdAt date greater than given timestamp (Tue, 02 Nov 2021 13:56:19 GMT)*

```
api/cargo?filter=[{"k":"status","c":"eq","v":[1,4]}]
```
*retrieve items that have status of 1 OR 4*

---

### **POST api/cargo/pay**

*Resolve SUSPENDED item of id -> PAID (will be then processed by customs automatically and will get DELIVERED)*

### body:
- `id: string`

---

### **POST api/cargo/return**

*Resolve SUSPENDED item of id -> RETURNED*

### body:
- `id: string`

